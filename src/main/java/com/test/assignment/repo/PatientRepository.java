package com.test.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.test.assignment.dto.Patients;

@Repository
public interface PatientRepository extends JpaRepository<Patients, Long> {
	@Query("SELECT p FROM Patients p")
	public List<Patients> getPatients();

	@Query("SELECT p FROM Patients p where name like %:name%")
	public List<Patients> getPatientByName(String name);
}
