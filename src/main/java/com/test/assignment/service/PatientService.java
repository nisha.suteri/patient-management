package com.test.assignment.service;

import java.util.List;

import com.test.assignment.dto.Patients;

public interface PatientService {
	/**
	 * Gets list of patients from table
	 * @return
	 */
	public List<Patients> getPatients();
	/**
	 * Gets patients matching input name
	 * @param name
	 * @return List of patients
	 * @throws BadParameterException 
	 */
	public List<Patients> getPatientByName(String name) throws BadParameterException;
	/**
	 * Gets patients by patient id
	 * @param id
	 * @return patient model
	 */
	public Patients getPatientById(Long id);
	/**
	 * saves new patient information into table
	 * @param patient
	 * @return
	 * @throws BadParameterException 
	 */
	public Patients savePatient(Patients patient) throws BadParameterException;
	/**
	 * Deletes p@Override
	atient from table
	 * @param id
	 * @return
	 * @throws PatientNotFoundException 
	 */
	public boolean deletePatient(Long id) throws PatientNotFoundException;
	/**
	 * 
	 * @param patient
	 * @return
	 * @throws PatientNotFoundException 
	 */
	public Patients updatePatient(Patients patient) throws PatientNotFoundException;
}
