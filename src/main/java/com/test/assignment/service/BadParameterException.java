package com.test.assignment.service;

public class BadParameterException extends Exception {
	public BadParameterException(String message) {
        super(message);
    }
}