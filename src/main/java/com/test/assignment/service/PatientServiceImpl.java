package com.test.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.assignment.dto.Patients;
import com.test.assignment.repo.PatientRepository;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	PatientRepository patientRepo;

	@Override
	public List<Patients> getPatients() {
		// TODO Auto-generated method stub
		return this.patientRepo.getPatients();
	}

	@Override
	public List<Patients> getPatientByName(String name) throws BadParameterException {
		// TODO Auto-generated method stub
		if (name != null)
			return this.patientRepo.getPatientByName(name);
		else
			throw new BadParameterException("Pass valid name in input");
	}

	@Override
	public Patients getPatientById(Long id) {
		return this.patientRepo.getOne(id);
	}

	@Override
	public Patients savePatient(Patients patient) throws BadParameterException {
		// TODO Auto-generated method stub
		if (patient.getName() != null && patient.getGender() != null) {
			return this.patientRepo.save(patient);
		} else
			throw new BadParameterException("Pass proper patient information");
	}

	@Override
	public boolean deletePatient(Long id) throws PatientNotFoundException {
		// TODO Auto-generated method stub
		if (this.patientRepo.getOne(id) != null) {
			this.patientRepo.deleteById(id);
			return true;
		} else {
			throw new PatientNotFoundException("No patients found for given id");
		}
	}

	@Override
	public Patients updatePatient(Patients patient) throws PatientNotFoundException {
		if (this.patientRepo.getOne(patient.getId()) != null) {
			return this.patientRepo.save(patient);
		} else {
			throw new PatientNotFoundException("No patients found for given id");
		}
	}

}
