package com.test.assignment.dto;

public enum Gender {

	FEMALE, MALE, OTHER
}
