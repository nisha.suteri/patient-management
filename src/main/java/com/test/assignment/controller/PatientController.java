package com.test.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.test.assignment.dto.Patients;
import com.test.assignment.service.BadParameterException;
import com.test.assignment.service.PatientNotFoundException;
import com.test.assignment.service.PatientService;

/**
 * 
 * @author Nisha Suteri
 *
 */
@RestController
public class PatientController {

	@Autowired
	PatientService patientService;
	
	@GetMapping("/")
	public ModelAndView viewHomePage() {

        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("listPatients", patientService.getPatients());

        //...

        return modelAndView;
    }
	
	@GetMapping("/searchpatient")
	public ModelAndView viewSearchPage() {

        ModelAndView modelAndView = new ModelAndView("searchpatient");
        return modelAndView;
    }
	
	@GetMapping("/addPatient")
    public ModelAndView showNewEmployeeForm(Model model) {
        // create model attribute to bind form data
        ModelAndView modelAndView = new ModelAndView("addpatient");
        modelAndView.addObject("patient", new Patients());
        return modelAndView;
    }
	
	@GetMapping("/v1/patients")
	public List<Patients> getPatients() {
		//log.info("Getting patients details from the database.");
		return patientService.getPatients();

	}
	@PostMapping("/v1/patient")
    public ModelAndView showNewEmployeeForm(@ModelAttribute("patient") Patients patient) throws BadParameterException {
        // create model attribute to bind form data
        ModelAndView modelAndView = new ModelAndView("redirect:/");
        modelAndView.addObject("patient", patientService.savePatient(patient));
        return modelAndView;
    }

	@PostMapping("/v1/patient/update")
	public Patients updatePatient(@RequestBody Patients patient) throws PatientNotFoundException {
		//log.info("Update patient details in the database.");
		return patientService.updatePatient(patient);
	}
	
	@GetMapping("/v1/patient/{id}")
    public ModelAndView getPatientById(@PathVariable(value = "id") long id) throws BadParameterException {

		ModelAndView modelAndView = new ModelAndView("updatepatient");
        modelAndView.addObject("patient", patientService.getPatientById(id));
        return modelAndView;

    }

	//delete mapping wasn't working with modelandview so workaround with get api.
	@GetMapping("/v1/deletepatient/{id}")
	public ModelAndView deletePatient(@PathVariable Long id) throws PatientNotFoundException {
		this.patientService.deletePatient(id);
		ModelAndView modelAndView = new ModelAndView("redirect:/");
		return modelAndView;
	}

	@GetMapping("/v1/searchpatient")
	public ModelAndView getPatientByName(@RequestParam(name = "search") String name) throws PatientNotFoundException, BadParameterException {
		System.out.println(name);
		ModelAndView modelAndView = new ModelAndView("searchpatient");
		modelAndView.addObject("listPatients", this.patientService.getPatientByName(name));
		
		return modelAndView;
	}
}
