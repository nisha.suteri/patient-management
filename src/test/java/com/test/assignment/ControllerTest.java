package com.test.assignment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.test.assignment.dto.Patients;
import com.test.assignment.repo.PatientRepository;
import com.test.assignment.service.PatientService;

class ServiceTest {
	@Mock
	private PatientRepository patientRepo;

	@InjectMocks
	private PatientService patientService;

	@Test
	public void testgetPatients() {
		List<Patients> patientMock = new ArrayList<Patients>();
		patientMock.add(new Patients(1L, "testname", "testgender", "7415434410"));
		Mockito.when(this.patientRepo.getPatients()).thenReturn(patientMock);
		assertEquals("testName", this.patientRepo.getPatients().get(0).getName());
	}
	
	@Test
	public void testgetPatientbyName() {
		List<Patients> patientMock = new ArrayList<Patients>();
		patientMock.add(new Patients(1L, "testname", "testgender", "7415434410"));
		Mockito.when(this.patientRepo.getPatients()).thenReturn(patientMock);
		assertEquals("testName", this.patientRepo.getPatients().get(0).getName());
	}
}
